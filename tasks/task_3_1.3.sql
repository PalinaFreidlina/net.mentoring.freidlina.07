USE Northwind;
GO
IF EXISTS
(
    SELECT *
    FROM sysobjects
    WHERE id = OBJECT_ID('dbo.Region')
          AND sysstat&0xf = 3
)
    EXEC sp_rename
         'dbo.Region',
         'Regions';
GO
IF COL_LENGTH('dbo.Customers', 'DateCreated') IS NULL
    ALTER TABLE [Northwind].[dbo].Customers
    ADD [DateCreated] DATE NULL;
--SELECT *
--FROM [Northwind].[dbo].Regions;
--SELECT *
--FROM [Northwind].[dbo].Customers;
USE Northwind;
GO
EXEC sp_rename
     'dbo.Regions',
     'Region';
GO
ALTER TABLE [Northwind].[dbo].Customers DROP COLUMN [DateCreated];

