--1
SELECT e.[EmployeeID],
       r.RegionDescription
FROM [Northwind].[dbo].[Employees] AS e
     JOIN [Northwind].[dbo].EmployeeTerritories AS et ON e.EmployeeID = et.EmployeeID
     JOIN [Northwind].[dbo].Territories AS t ON et.TerritoryID = t.TerritoryID
     JOIN [Northwind].[dbo].Region AS r ON t.RegionID = r.RegionID
WHERE r.RegionDescription = 'Western';
--2
SELECT c.CustomerID,
       COUNT(o.CustomerID) AS 'Orders count'
FROM [Northwind].[dbo].Customers AS c
     LEFT JOIN [Northwind].[dbo].Orders AS o ON c.CustomerID = o.CustomerID
GROUP BY c.CustomerID
ORDER BY [Orders count];
