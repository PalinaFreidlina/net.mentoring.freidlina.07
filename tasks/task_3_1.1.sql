GO
IF NOT EXISTS
(
    SELECT *
    FROM sysobjects
    WHERE id = OBJECT_ID('dbo.CreditCards')
          AND sysstat&0xf = 3
)
    CREATE TABLE [Northwind].[dbo].[CreditCards]
([CreditCardID]     INT IDENTITY(1, 1) NOT NULL,
 [CreditCardNumber] NVARCHAR(16) NOT NULL,
 [EmployeeID]       INT NOT NULL,
 [CardHolder]       NVARCHAR(40) NULL,
 [ExpirationDate]   DATE NULL,
 CONSTRAINT [PK_CreditCards] PRIMARY KEY([CreditCardID] ASC),
 CONSTRAINT [FK_CreditCards_Employees] FOREIGN KEY([EmployeeID]) REFERENCES [Northwind].[dbo].[Employees]([EmployeeID])
);
--GO
--INSERT INTO [Northwind].[dbo].CreditCards
--([CreditCardNumber],
-- [EmployeeID],
-- [CardHolder],
-- [ExpirationDate]
--)
--VALUES
--(N'1234567890987654',
-- 2,
-- N'John Green',
-- {d '1998-05-06'}
--);
--SELECT *
--FROM [Northwind].[dbo].[CreditCards];
GO
DROP TABLE [Northwind].[dbo].[CreditCards];