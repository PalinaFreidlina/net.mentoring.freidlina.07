--1
SELECT [CustomerID],
       [Country]
FROM [Northwind].[dbo].[Customers]
WHERE [Country] IN('USA', 'Canada')
ORDER BY [CustomerID],
         [Country];
--2
SELECT [CustomerID],
       [Country]
FROM [Northwind].[dbo].[Customers]
WHERE [Country] NOT IN('USA', 'Canada')
ORDER BY [CustomerID];
--3
SELECT DISTINCT
       [Country]
FROM [Northwind].[dbo].[Customers];
