--1
SELECT [OrderID],
       [ShippedDate],
       [ShipVia]
FROM [Northwind].[dbo].[Orders]
WHERE [ShippedDate] >= {d '1998-05-06'}
      AND [ShipVia] >= 2;
--2
SELECT [OrderID],
       CASE
           WHEN [ShippedDate] IS NULL
           THEN 'Not Shipped'
           ELSE CONVERT(VARCHAR, [ShippedDate], 121)
       END AS [ShippedDate]
FROM [Northwind].[dbo].[Orders]
WHERE [ShippedDate] IS NULL;
--3
SELECT [OrderID] AS 'Order number',
       ISNULL(CONVERT(VARCHAR, [ShippedDate], 121), 'Not shipped') AS 'Shipped date'
FROM [Northwind].[dbo].[Orders]
WHERE [ShippedDate] > {d '1998-05-06'}
      OR ([ShippedDate] IS NULL);
	   