
SELECT Year([OrderDate]) As 'Year', Count(*) As 'Total'
  FROM [Northwind].[dbo].[Orders]
  GROUP BY Year([OrderDate])

SELECT LastName + ' ' + FirstName as 'Seller', Count(*) As 'Amount'
  FROM [Northwind].[dbo].[Orders] as o, [Northwind].[dbo].[Employees] as e
  WHERE o.EmployeeID = e.EmployeeID
  GROUP BY o.EmployeeID, LastName + ' ' + FirstName
  ORDER BY Amount desc

SELECT [EmployeeID], [CustomerID], COUNT(*) As 'Amount'
  FROM [Northwind].[dbo].[Orders]
  WHERE Year([OrderDate]) = 1998
  GROUP BY CustomerID, EmployeeID

SELECT [EmployeeID], [CustomerID], e.[City]
  FROM [Northwind].[dbo].[Employees] as e, [Northwind].[dbo].[Customers] as c
  WHERE e.City = c.City

SELECT  [City], Count(*) As 'Customers Count'
  FROM [Northwind].[dbo].[Customers]
  GROUP BY City
  HAVING Count(*) > 1 

SELECT [EmployeeID], [ReportsTo]
  FROM [Northwind].[dbo].[Employees]