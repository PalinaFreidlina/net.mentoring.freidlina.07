--1
SELECT DISTINCT
       [OrderID]
FROM [Northwind].[dbo].[Order Details]
WHERE [Quantity] BETWEEN 3 AND 10;
--2
SELECT [CustomerID],
       [Country]
FROM [Northwind].[dbo].[Customers]
WHERE [Country] BETWEEN 'b%' AND 'h%'
ORDER BY [Country];
--3
SELECT [CustomerID],
       [Country]
FROM [Northwind].[dbo].[Customers]
WHERE [Country] LIKE '[b-g]%';