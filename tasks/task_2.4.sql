SELECT CompanyName
FROM [Northwind].[dbo].Suppliers
WHERE SupplierID IN
(
    SELECT SupplierID
    FROM [Northwind].[dbo].Products
    WHERE UnitsInStock = 0
);

SELECT EmployeeID
FROM [Northwind].[dbo].Employees
WHERE EmployeeID IN
(
    SELECT o.EmployeeID
    FROM [Northwind].[dbo].Orders AS o
    WHERE
(
    SELECT COUNT(*)
    FROM [Northwind].[dbo].Orders
    WHERE EmployeeId = o.EmployeeID
) > 150
);

SELECT c.CustomerID
FROM [Northwind].[dbo].Customers AS c
WHERE NOT EXISTS
(
    SELECT CustomerID
    FROM [Northwind].[dbo].Orders
    WHERE CustomerID = c.CustomerID
);
