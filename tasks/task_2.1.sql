
SELECT SUM(([UnitPrice]-[UnitPrice]*[Discount])*[Quantity]) as 'Totals'
  FROM [Northwind].[dbo].[Order Details]

SELECT (COUNT(*)-COUNT([ShippedDate]))
  FROM [Northwind].[dbo].[Orders]

SELECT COUNT(distinct [CustomerID])
  FROM [Northwind].[dbo].[Orders]